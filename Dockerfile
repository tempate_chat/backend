FROM node:alpine
WORKDIR /app
COPY . .
VOLUME /app/files
RUN npm i 
EXPOSE 3000
CMD ["node","server.js"]
