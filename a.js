const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
  console.log(socket);
  console.log('a user connected');
  let token = socket.handshake.query.token;
  console.log(token);
  setInterval(() => {
    io.emit('msg',{data:[1,2]})
  }, 1000);
});

http.listen(8080, () => {
  console.log('listening on *:3000');
});