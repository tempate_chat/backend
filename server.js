import config from './src/config/index.js'
import cors from 'cors'
import express from 'express'
import bodyParser from 'body-parser'
import { Server } from 'socket.io'
import { router } from './src/routers/index.js'
import { checkToken } from './src/helps/jwt.js'
const app = express()

// # Allow preflight from browser
app.use(cors())

// # Set maxium payload
app.use(bodyParser.urlencoded({ extended: false }))

// # set paylosd from json
app.use(bodyParser.json())

// app.get('/', (req, res) => {
//   socket.emit('aa','ok')
//   res.sendFile(process.cwd() + '/index.html')
// })

// # middleware blok request is not authorization (access token)
app.use((req, res, next) => {
  if (req.path == '/api/v1/login') {
    next()
  } else {
    if ('authorization' in req.headers && checkToken(req.headers.authorization)) {
      next()
    } else {
      res.status(401).send({ status: 'fail', data: '', message: 'Unauthorized' })
    }
  }
})

// # main routing
app.use('/api', router)

// # handel error methods
app.route('*').all((req, res) => {
  res.status(404).send({ status: 'fail', data: '', message: 'Method not found' })
})

console.log(Server)
// # Start app
var server = app.listen(config.PORT, () => console.log(`app listening on port ${config.PORT}`))

// # cors origin of socket.io
// https://stackoverflow.com/questions/24058157/socket-io-node-js-cross-origin-request-blocked

var io = new Server(server, {
  cors: {
    origin: '*',
  },
})

var user_connect = []

// setInterval(() => {
//   io.to('room1').emit('ss')
// }, 3000)

// # Step connection on server-client
// TODO Connect
// 1.event 'joinRoom'
// 2.join room(sid)
// 3.append user(sdi,username) connect in list user

// TODO Disconnect
// 1.remove user(sid,username) in list user
// 2.leave room(sid)

// TODO notify (schdule)
// 1.count user in user list
// 2.get noti each user
// 3.emit message each room(sid) of user

var clientaConnect = []
function updateClient(){
  
}

io.on('connection', (socket) => {
  var total = io.engine.clientsCount
  console.log(total)

  console.log(Object.keys(io.engine.clients))
  // console.log(io.engine.clients);

  // socket.emit('getCount', total)
  console.log(`a user id ${socket.id} is connected `)

  socket.on('disconnecting', (reason) => {
    console.log(reason)
    console.log(socket.rooms)
    // console.log('user disconnected')
  })

  socket.on('message', (val) => {
    console.log(val)
    // socket.emit('msgFromRoom' , val)
    // socket.broadcast.to(val.room).emit('msg')
    socket.join(val.room)
    io.to(val.room).emit('message', val.room)
  })

  setInterval(() => {
    io.to('room1').emit('message', '1')
  }, 5000)
  // app.get('/', (req, res) => {
  //   io.emit('aa','ok')
  //   res.sendFile(process.cwd() + '/index.html')
  // })

  // setInterval(() => {
  //   socket.emit('a','fromserver')
  // }, 5000);
  // socket.on('roomMsg', (msg) => {
  //   io.emit('message from room :  ' + msg)
  // })

  socket.on('a', (msg) => {
    console.log('message: ' + msg)
    io.emit('a', 'fromserver')
  })
})
