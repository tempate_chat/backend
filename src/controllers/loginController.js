import readUser from '../models/login.js'
import { genToken } from '../helps/jwt.js'
// TODO --------------------------
// 1.get request from front-end
// 2.read user.json file
// 3.diff user from front-end and user.json file
// 4.where same user and pass generate jwt Token
// TODO --------------------------

function login(req, res) {
  readUser((user) => {
    if (req.query.username === user.username && req.query.password === user.password) {
      res.status(200).send({
        status: 'success ',
        message: '',
        data: { token: genToken({ user: user.username }) },
      })
    } else {
      res.status(401).send({ status: 'fail ', message: 'Unauthorized', data: '' })
    }
  })
}

export { login }
