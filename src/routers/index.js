import express from 'express'
import { login } from '../controllers/loginController.js'
import { getCamera, updateCamera } from '../controllers/cameraController.js'

const router = express.Router()

router.get('/v1/login', login)
router.route('/v1/camera').get(getCamera).post(updateCamera)
router.route('/v1/camera').get(getCamera).post(updateCamera)

export { router }
