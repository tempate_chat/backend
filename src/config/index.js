const config = {
  PORT: process.env.PORT || 3000,
  KEY: process.env.KEY || '53837757c7fac25afb9092255fb4268218b35a97',
  PATH_USER : process.env.PATH_USER || '/files/'
}

export default config
