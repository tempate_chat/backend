import config from '../config/index.js'
import base64url from 'base64url'
import sha256 from 'js-sha256'

function genToken(payload) {
  // # Function ในการ Generate jwt Token
  const _header = base64url(JSON.stringify({ alg: 'sha256', typ: 'jwt' }))

  // # ทำการ สร้าง exp (ระยะเวลาหมดอายุ เป็น unix time) และ iat (เวลาที่สร้าง เป็น unix time)
  const _date = { exp: ((Date.now() / 1000) | 0) + 60 * 60, iat: (Date.now() / 1000) | 0 }

  // # merge exp date in payload
  const _payload = base64url(JSON.stringify({ ...payload, ..._date }))

  // # Generate signature
  const _signature = sha256(_header + '.' + _payload + '.' + config.KEY)

  // # return access token
  return _header + '.' + _payload + '.' + _signature
}

function checkToken(token) {
  // # เป็น Function สำหรับการตรวจสอบว่า Access Token ที่ออกไปให้นั้น มีการแก้ไข หรือ หมดอายุไปหรือยัง

  // # Split Data (header , payload ,signature)
  const _token = token.split('.')

  // # Generate New Token with Differnce jwt
  const _newToken = sha256(_token[0] + '.' + _token[1] + '.' + config.KEY)

  // # Extrack payload from jwt
  const _payload = JSON.parse(base64url.decode(_token[1]))

  // # check diff token and Exp date
  if (_token[2] === _newToken && _payload.exp >= ((Date.now() / 1000) | 0)) {
    return { status: true, data: _payload }
  } else {
    return { status: false, data: '' }
  }
}

export { genToken, checkToken }
