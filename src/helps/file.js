import config from '../config/index.js'
import fs from 'fs'

const __dirname = process.cwd()

function readFile(filename, callback) {
  fs.readFile(__dirname + config.PATH_USER + filename, (err, data) => {
    if (err) throw err
    callback(JSON.parse(data))
  })
}

function writeFile(newdata, filename, callback) {
  readFile(filename, (olddata) => {
    var data = JSON.stringify({ ...olddata, ...newdata })
    fs.writeFile(__dirname + config.PATH_USER + filename, data, (err) => {
      if (err) throw err
      callback('Save Success !!')
    })
  })
}

export { readFile, writeFile }
