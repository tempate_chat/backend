import { readFile, writeFile } from '../helps/file.js'

function readCamera(callback) {
  readFile('config.json', (data) => {
    callback(data.camera)
  })
}

function writeCamera(val, callback) {
  writeFile(val, 'config.json', (data) => {
    callback(data)
  })
}

export { readCamera, writeCamera }
